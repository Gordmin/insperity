<footer class='bg-gray py-4'>
    <div class="container-fluid sw">
        <div class="row">
                <div class="col-lg-6 col-md-12">
                    <p class="mb-0 text-lg-left text-md-center text-sm-center weight-light text-smaller">Copyright &copy; <?php echo date("Y"); ?> Insperity. All Rights Reserved.</p>
                </div>
                <div class="col-lg-3 offset-lg-3 col-md-12">
                    <p class="mb-0 text-lg-right text-md-center text-sm-center weight-light text-smaller">Call: <a class="tel-footer" href="tel:1-855-677-7813">855.677.7813</a></p>
                </div>
        </div>
    </div>
</footer>
