<div class="container-fluid mt-4 mb-5 ">
    <div class="row">
        <div class="col-lg-6 col-md-12 bg-img"></div>
        <div class="col-lg-6 col-md-12 bg-gray py-5">
            <div class="row">
                <div class="offset-xl-1 col-xl-7 col-lg-12 col-md-12 col-sm-12">
                    <h2 class="blue">About Insperity</h2>
                    <p>Since 1986, Insperity has provided industry-leading human resources management products and services to over 100,000 businesses. With our comprehensive PEO service your business gets access to:</p>
                    <ul>
                        <li>Fortune 500-level employee benefits</li>
                        <li>Stress-free payroll and HR administration</li>
                        <li>HR-related government compliance</li>
                        <li>Ongoing health care reform support</li>
                        <li>A dedicated HR service team</li>
                    </ul>
                    <p class="blue text-1-15 mt-4 weight-medium"><a href="">See all of Insperity's services &#x2192;</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
