<section class="amazing-clients container-fluid sw my-4 mx-auto">
    <p class="mb-3 text-smaller text-center">Some of our amazing clients</p>
    <div class="row d-flex align-items-center">
        <div class="col-md-4">
            <img class="img-fluid d-block mx-auto" src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-jib-jab.jpg" alt="Jib Jab">
        </div>
        <div class="col-md-4">
            <img class="img-fluid d-block mx-auto" src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-marketingprofs.jpg" alt="MarketingProfs">
        </div>
        <div class="col-md-4">
            <img class="img-fluid d-block mx-auto" src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-ronald-regan.jpg" alt="Ronald Regan">
        </div>
    </div>
</section>
