<aside class="col-sm-3">
  <?php include Wrapper\sidebar_path(); ?>
  <div class="row">
      <div class="col-12 bg-green">
          <img class="img-fluid flex d-block mx-auto" src="<?php echo get_template_directory_uri(); ?>/dist/images/book-front.png" alt="HR Outsourcing: A Step-by-Step Guide to Professional Employer Organizations (PEOs)">
      </div>
  </div>
  <div class="row">
      <div class="col-12 contact-form bg-dk-gray">
          Contact form placement. This is where the contact form will go.
      </div>
  </div>

</aside>
