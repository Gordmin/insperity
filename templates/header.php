<header class="banner bottom-shadow py-3 z10">
    <div class="container-fluid sw">
        <div class="row">
            <div class="col-md-6">
                <a class="brand" href="<?= esc_url(home_url('/')); ?>"><img class="img-fluid mw225 mb-md-0 mb-4 d-block" src="<?php echo get_template_directory_uri(); ?>/dist/images/logo-insperity.png" alt="<?php bloginfo('name'); ?>"></a>
            </div>
            <div class="col-md-6 phone">
                <p class="mb-1 text-smaller lh100">Speak to a specialist now</p>
                <h2 class="blue lh100 mb-0"><span class="icon-phone"><a class="tel" href="tel:1-855-667-7813">855.677.7813</a></span></h2>
            </div>
        </div>
    </div>
</header>
