<?php use Roots\Sage\Titles; ?>

<div class="page-header">
  <h1 class="blue mt-5 mb-0"><?= Titles\title(); ?></h1>
</div>
