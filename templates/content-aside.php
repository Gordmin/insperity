<aside class="offset-xl-1 col-xl-3 col-lg-5 col-md-5 col-sm-12 border-top-0">
    <div class="row">
      <div class="col-12 bg-green">
          <img class="img-fluid flex d-block mx-auto mt-3" src="<?php echo get_template_directory_uri(); ?>/dist/images/book-front.png" alt="HR Outsourcing: A Step-by-Step Guide to Professional Employer Organizations (PEOs)">
          <h5 class="mb-4 mt-2 text-center white">Download your free eBook</h5>
      </div>
    </div>
    <div class="row">
      <div class="col-12 contact-form bg-dk-gray no-top-border">
          <iframe id="form-iframe" onload="iframeLoaded()" src="<?php echo get_template_directory_uri(); ?>/landing-form.php"></iframe>
          <p class="text-center text-smallest mt-2 mb-3 lh100">1024 e-books downloaded.</p>
      </div>
    </div>
    <script>
      function iframeLoaded() {
          var iFrameID = document.getElementById('form-iframe');
          if(iFrameID) {
                iFrameID.height = "";
                iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
          }
      }
    </script>
</aside>
