<html>
    <head>
        <link rel="stylesheet" href="form-styles.css" type="text/css">
    </head>
    <body>
        <img class="img-fluid icon-arrow-down-green mb-2" src="dist/images/icon-arrow-down-green.png" alt="Download your free e-book!">

        <form action="form-handler.php" name="peos_form" method="post">
            <label class="weight-normal text-smallest roboto">Email</label> <input class="text-smallest roboto rounded-5" type="text" name="email" required><br>
            <label class="weight-normal text-smallest roboto">First Name</label> <input class="text-smallest roboto rounded-5" type="text" name="f_name" required><br>
            <label class="weight-normal text-smallest roboto">Last Name</label> <input class="text-smallest roboto rounded-5" type="text" name="l_name" required><br>
            <label class="weight-normal text-smallest roboto">Company Name</label> <input class="text-smallest roboto rounded-5" type="text" name="c_name" required><br>
            <label class="weight-normal text-smallest roboto">Phone</label> <input class="text-smallest roboto rounded-5 mb-1" type="text" name="phone" required><br>
            <input id="submit" class="btn-gold roboto" type="submit" value="Download e-book &#9658;">
        </form>
    </body>
</html>
