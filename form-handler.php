<link rel="stylesheet" href="form-styles.css" type="text/css">
<img class="img-fluid icon-arrow-down-green mb-2" src="dist/images/icon-arrow-down-green.png" alt="Download your free e-book!">
<section>
    <label class="weight-normal text-smallest roboto">Email</label> <span class="response"><?php echo $_POST["email"]; ?></span><br>
    <label class="weight-normal text-smallest roboto">First Name</label> <span class="response"><?php echo $_POST["f_name"]; ?></span><br>
    <label class="weight-normal text-smallest roboto">Last Name</label> <span class="response"><?php echo $_POST["l_name"]; ?></span><br>
    <label class="weight-normal text-smallest roboto">Company Name</label> <span class="response"><?php echo $_POST["c_name"]; ?></span><br>
    <label class="weight-normal text-smallest roboto">Phone</label> <span class="response"><?php echo $_POST["phone"]; ?></span>
</section>
<p class="lh120">The PDF will load in a new window 3 seconds</p>
<script>
setTimeout(function() {
    window.open(
      'pdfs/2017-Benefits-at-a-Glance---Insperity-Corporate-Employees.pdf',
      '_blank'
    );
}, 3000);
</script>
