<?php get_template_part('templates/page', 'header'); ?>

    <div class="alert alert-warning bg-dk-gray">
        <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
    </div>

<?php get_search_form(); ?>
